const electron = require('electron');
const app = electron.app;

const path = require('path');
const url = require('url');

const BrowserWindow = electron.BrowserWindow;

//let mainWindow;

app.on('ready', () => {
   var mainWindow = new BrowserWindow({width: 500, height: 430});

   mainWindow.setTitle('Encrypter');
  
   /* es lo mismo que esta en la siguiente sentencia
   mainWindow.loadURL(url.format({
      pathname: path.join(__dirname,'/Crypto 2.0/index.html'),
      protocol: 'file',
      slashes: true
   }));*/

   mainWindow.loadURL('file://'+ __dirname +'/Crypto 2.0/index.html');

   mainWindow.on('closed', () => {
      mainWindow = null;
   });
});
